var timer_block;
var word_block;

(async () => {
    word_block = (await browser.storage.sync.get({ word_block: true })).word_block;
    timer_block = (await browser.storage.sync.get({ timer_block: false })).timer_block;
})();
browser.storage.onChanged.addListener((changes, area) => {
    if (area !== "sync") return;
    if (typeof changes.word_block !== 'undefined') word_block = changes.word_block.newValue;
    if (typeof changes.timer_block !== 'undefined') timer_block = changes.timer_block.newValue;
});

browser.runtime.onMessage.addListener((message, sender) => {
    if (typeof message.block_type === 'undefined') return;
    return Promise.resolve({ timer_block: timer_block, word_block: word_block });
});
