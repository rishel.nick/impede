var panic_end = 0;

document.getElementById('impede__sleep').addEventListener("submit", e => {
    e.preventDefault();

    if (new Date().getTime() < panic_end) return;

    var sleep_duration = document.querySelector('select').value;
    var sleep_end = new Date().getTime() + (sleep_duration * 60 * 1000);
    browser.runtime.sendMessage({ sleep: {}, end_time: sleep_end });
    e.target.reset();

    var end_time_node = document.querySelector('span');
    end_time_node.childNodes.forEach(n => n.remove());
    if (sleep_duration != 0) {
        var end_hours_minutes = new Date(sleep_end).toLocaleTimeString([],
            { hour: '2-digit', minute: '2-digit' });
        end_time_node.textContent = "Sleeping until " + end_hours_minutes;
    }
});

document.getElementById('impede__panic').addEventListener("click", () => {
    panic_end = new Date().getTime() + (15 * 60 * 1000);
});

const stor = browser.storage.sync;
var select_node = document.getElementById('impede__word_length_select');
stor.get({ word_length: 3 }).then(
    result => {
        const word_len = result.word_length;
        for (var i = 3; i <= 12; i++) {
            const selected = i == word_len;
            select_node[select_node.options.length] = new Option(i, i, false, selected);
        }
        select_node.addEventListener("change", e => {
            stor.set({ word_length: e.target.value });
        });
    }
)

browser.runtime.sendMessage({ block_type: {} }).then(
    blocks => {
        var word_block_node = document.getElementById('word_block');
        word_block_node.checked = blocks.word_block;
        word_block_node.addEventListener("change", _ => {
            stor.set({ word_block: word_block_node.checked });
        });

        var timer_block_node = document.getElementById('timer_block');
        timer_block_node.checked = blocks.timer_block;
        timer_block_node.addEventListener("change", _ => {
            stor.set({ timer_block: timer_block_node.checked });
        });
    }
);
