var open_request = indexedDB.open("words", { version: 1 });
var order = Promise.resolve(); // used to force correct ordering of callbacks with interal async
var ready_resolve;
var ready = new Promise(resolve => ready_resolve = resolve);

var word_len;
(async () => {
    word_len = (await browser.storage.sync.get({ word_length: 3 })).word_length;
})();
browser.storage.onChanged.addListener((changes, area) => {
    if (area !== "sync") return;
    if (typeof changes.word_length === 'undefined') return;
    word_len = changes.word_length.newValue;
});

browser.runtime.onMessage.addListener(message => {
    if (typeof message.get_word === 'undefined') return;
    return new Promise(async (resolve) => {
        console.log("Sending word");
        // indexeddb transactions time out when there are no pending requests at the end of
        // the javascript event loop, as is the case when awaiting on anything aside from an
        // indexeddb callback, so frontload awaits

        var db = (await ready).target.result;
        var store = db.transaction("words", "readonly").objectStore("words");
        var index = store.index("length");

        var range = IDBKeyRange.only(Number(word_len));
        var count = new Promise(resolve => {
            index.count(range).onsuccess = resolve;
        });

        var advanced = false;
        index.openCursor(range).onsuccess = async (evt) => {
            var cursor = evt.target.result;

            var steps = Math.floor(Math.random() * (await count).target.result);
            if (!advanced && steps > 0) {
                cursor.advance(steps); // will trigger another call to onsuccess
                advanced = true;
            } else {
                console.log("Sent the word \"" + cursor.value.word + "\"");
                resolve({ word: cursor.value.word });
            }
        };
    });
});

var success = evt => {
    order.then(ready_resolve(evt));
};
var onupgradeneeded = evt => {
    var wordsFile = fetch(browser.extension.getURL("words.txt")).then(file => file.text());
    order = order.then(async () => {
        console.log("Setting up words database.");
        var db = evt.target.result;
        var store = db.createObjectStore("words", { autoIncrement: true });
        store.createIndex("length", "length", { unique: false });
        await (() => new Promise(resolve =>
            store.transaction.oncomplete = resolve
        ))();

        var wordsString = await wordsFile;
        var words = wordsString.split("\n");

        console.log("Writing to database.");
        var word_store = db.transaction("words", "readwrite").objectStore("words");
        words.forEach(word => word_store.add({ word: word, length: word.length }));
        console.log("Done writing");
    });
};

open_request.onsuccess = success;
open_request.onupgradeneeded = onupgradeneeded;
open_request.onerror = () => {
    console.log("IndexedDB error while openning \"words\", rebuilding... ");
    var delete_request = indexedDB.deleteDatabase("words");
    delete_request.onsuccess = () => {
        console.log("Successfully deleted \"words\" database")
        var repaired_request = indexedDB.open("words", { version: 1 });
        repaired_request.onsuccess = success;
        repaired_request.onupgradeneeded = onupgradeneeded;
        repaired_request.onerror = () => {
            console.log("Error opening \"words\" from IndexedDB after rebuild, giving up.");
        };
    };
    delete_request.onerror = () => {
        console.log("Error deleting \"words\" from IndexedDB, giving up.");
    };
};
