var check = browser.runtime.sendMessage({ check_url: {}, url: document.URL });
var get_word = browser.runtime.sendMessage({ get_word: {} });
var block_type = browser.runtime.sendMessage({ block_type: {} });

var config = { childList: true, attributes: false, subtree: false };
var observer = new MutationObserver(mutationList => {
    for (const mutation of mutationList) {
        if (mutation.type !== 'childList') continue;
        for (const node of mutation.addedNodes) {
            if (node.localName !== 'body') continue;
            check.then(() => impede(node));
        }
    }
});
observer.observe(document.documentElement, config);

function appendNewDiv(parent, id) {
    var new_div = document.createElement('div');
    new_div.id = id;
    parent.appendChild(new_div);
    return new_div;
}

async function impede(body_node) {
    var div_overlay = appendNewDiv(body_node, 'impede__overlay');
    var div_center = appendNewDiv(div_overlay, 'impede__center');

    var blocks = await block_type;
    if (blocks.timer_block === true) {
        impede_timer(div_overlay, div_center);
    }
    if (blocks.word_block === true) {
        impede_word(body_node, div_overlay, div_center);
    }
}

async function impede_timer(div_overlay, div_center) {
    var div_message = appendNewDiv(div_center, 'impede__timer_instructions');
    div_message.textContent = "In two minutes, pages wil be unlockable for 15 minutes";

    var button_sleep = document.createElement('input');
    button_sleep.setAttribute('disabled', 'disabled');
    button_sleep.type = 'button';
    button_sleep.value = "sleep 15 minutes";
    button_sleep.onclick = () => {
        var sleep_end = new Date().getTime() + (15 * 60 * 1000);
        browser.runtime.sendMessage({ sleep: {}, end_time: sleep_end });
        document.body.removeChild(div_overlay);
    }; 
    div_center.appendChild(button_sleep);

    var pending = 0;
    var unblock_wait = async () => {
        pending++;
        await new Promise(resolve => setTimeout(resolve, 2 * 60 * 1000));
        pending--;
        if (pending !== 0) return;
        button_sleep.removeAttribute('disabled');
    }

    // tab change
    document.addEventListener("visibilitychange", async () => {
        button_sleep.setAttribute('disabled', 'disabled');
        await unblock_wait();
    });
    // window lost focus
    window.onblur = async () => {
        button_sleep.setAttribute('disabled', 'disabled');
        await unblock_wait();
    };
    window.onfocus = async () => {
        button_sleep.setAttribute('disabled', 'disabled');
        await unblock_wait();
    };
    await unblock_wait();
}

async function impede_word(body_node, div_overlay, div_center) {
    var div_prompt = appendNewDiv(div_center, 'impede__prompt');
    var span_formula = document.createElement('span');
    div_prompt.appendChild(span_formula);

    var input_result = document.createElement('input');
    input_result.type = "number";
    div_prompt.appendChild(input_result);

    var div_alphabet = appendNewDiv(div_center, 'impede__alphabet');

    const alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    const kv = Array.from(alphabet, a => [a, Math.floor(Math.random() * 9) + 1]); // 1-9

    for (const k of kv) {
        var div_kv = document.createElement('div');
        div_kv.className = "kv";
        var div_key = document.createElement('div');
        var div_value = document.createElement('div');

        div_key.textContent = k[0];
        div_value.textContent = k[1];

        div_kv.appendChild(div_key);
        div_kv.appendChild(div_value);
        div_alphabet.appendChild(div_kv);
        // add an empty space for cleaner alignment
        if (k[0] == "f") div_alphabet.appendChild(document.createElement('div'));
    }

    const map = new Map(kv);
    var word = (await get_word).word;
    var result = word.split("").map(i => map.get(i)).reduce((acc, val) => acc + val);
    var word_formula = word.split("").reduce((acc, str) => acc + " + " + str);
    span_formula.textContent = word_formula;

    input_result.onchange = () => {
        if (input_result.value == result) {
            document.body.removeChild(div_overlay);

            // setup a listener in case the page uses history.pushState()
            var navCb = message => {
                if (typeof message.navigation === 'undefined') return;
                browser.runtime.onMessage.removeListener(navCb);
                check = browser.runtime.sendMessage({ check_url: {}, url: document.URL });
                get_word = check.then(() => browser.runtime.sendMessage({ get_word: {}, length: 3 }));
                check.then(() => impede(body_node));
            };
            browser.runtime.onMessage.addListener(navCb);
        }
    };

}
