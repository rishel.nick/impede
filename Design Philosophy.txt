What is Impede

[add note of xkcd blag on rebooting pc]
[main theme moving from unconscious to conscious decisions][aka more intentional actions][motivation, need a paragraph to describe]
[lead into activation energy as a concept]
[lead into the knife edge to walk]
[end with moral vs immoral design, user engagement without context is the latter][lead into comparative game screens, Overwatch vs Rocket League]

Impede can be thought of as an anti-"Don't Make Me Think". Impede is a WebExtension for Firefox which overlays a simple math puzzle over "impeded" webpages in order to inconvenience browsing them habitually.

Impede approaches the problem of habitually browsing websites not as a problem requiring force of will avoidance, instead as activities subconsciously overweight in importance due to their ease of access.

Increasing the "activation energy" for habits

Activation energy is a term from chemistry meaning the minimum amount of energy that is required to start a reaction. Borrowing this term, activation energy as applied to design is the minimum energy necessary to interact with content; in Impede's case the content being a webpage.

Designers seek to minimize activation energy to increase user engagement. Unfortunately for the user this race toward maximum engagement favors passive over active engagement and reactivity actions instead of thoughtful ones. As with all tragedies of the commons designers can't be expected to solve this problem as trying to respect users attention opens the door for competitors who don't. Instead this must be solved on the users side.

The goal of Impede was to inconvenience access to webpages.

It's important to note the goal is not to abstain from using those webpages, just to make unconsciously navigating to them difficult. Hard blocking of such content will inevitably lead to an attempt to circumvent the blocker, likely by disabling the plugin. In the case of Impede it was important that going through its prompt is difficult enough to often prompt the question whether or not you want to do it, but not be so difficult that the app is disabled "temporarily". Impede had to straddle a fine line between feeling involved enough to break unconscious action and prompting reflection of what you are doing, while simultaneously feeling like the easiest path to take.

Make escape hatches which are off the "happy path"

[gif of sleeping Impede]

A reoccurring theme of Impede is that it attempts to inconvenience action while remaining the easiest way to navigate to the page when consciously desired. This is often handled through careful management of context switching. When unlocking a webpage your choices are to do the math prompt which has no context switching since your focus never leaves the main page, or to disable the plugin for the impeded webpage and refreshing which requires three context switches - from the page to the page lock button, to the refresh button, back to the webpage, plus two actions to re-enable Impede for the webpage if they choose to do so. This works for the common case where the user acknowledges the inconvenience as acceptable since they're messing around in the first place, but it does not work when trying to do real work that requires access to impeded webpages. For cases where it would get in the way of accessing pages necessary for work, Impede needed an escape hatch. This escape hatch needed to take into account the same balance of inconvenience, as it could easily become the habitual method for unlocking webpages. For this reason it was important to get it off the block page itself. The option to sleep Impede was placed in options for this reason. It was out of the  way, but not so out of the way that it's likely to be avoided when it's the right tool to use. The sleep method was set up as 3 timeframes Impede will sleep for: 5, 15, and 30 minutes. This was chosen intentionally to prevent increasing activation energy of using this method as having too many options or user defined options would increase the mental overhead. Imporantly it also takes away the overhead of re-enabling Impede after the fact.

