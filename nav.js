browser.webNavigation.onHistoryStateUpdated.addListener(
    details => {
        if (details.transitionType != "link") return;
        // common case as content script will normally not be listening, so ignore
        browser.tabs.sendMessage(details.tabId, { navigation: {} }).catch(() => { });
    }
);
