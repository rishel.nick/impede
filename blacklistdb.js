const stor = browser.storage.sync;
var sleep_end = 0;

browser.browserAction.onClicked.addListener(tab => {
    const domain = new URL(tab.url).hostname;
    const key = domain.split('.').join('');
    stor.get(key).then(
        result => {
            if (Object.keys(result).length > 0) {
                console.log("Removing %O from the blacklist", key);
                stor.remove(key).catch(() => console.log("blacklist removal failed"));
                updateIconUnimpeded(tab.id, domain);
            } else {
                const kv = { [key]: null };
                console.log("Adding %O to the blacklist", kv);
                stor.set(kv).catch(() => console.log("blacklist addition failed"));
                updateIconImpeded(tab.id, domain);
            }
        },
        error => console.log(error)
    );
});

browser.runtime.onMessage.addListener((message, sender) => {
    if (typeof message.check_url !== 'undefined') {
        return new Promise((resolve, reject) => {
            const domain = new URL(message.url).hostname;
            const key = domain.split('.').join('');
            stor.get(key).then(
                result => {
                    if (Object.keys(result).length > 0) {
                        updateIconImpeded(sender.tab.id, domain);
                        if (new Date().getTime() > sleep_end) {
                            resolve();
                        } else {
                            reject();
                        }
                    } else {
                        updateIconUnimpeded(sender.tab.id, domain);
                        reject();
                    }
                },
                error => console.log(error)
            );
        });
    } else if (typeof message.sleep !== 'undefined') {
        sleep_end = message.end_time;
    }
});

function updateIconImpeded(tabId, domain) {
    browser.runtime.getPlatformInfo(info => {
        if (info.os === browser.runtime.PlatformOs.ANDROID) return;
        var slowIcons;
        if (new Date().getTime() > sleep_end) {
            slowIcons = { 16: "icons/slow-16.png", 32: "icons/slow-32.png" };
        } else {
            slowIcons = { 16: "icons/slow-sleep-16.png", 32: "icons/slow-sleep-32.png" };
        }
        browser.browserAction.setIcon({ tabId: tabId, path: slowIcons });
    });
    var is_sleeping = new Date().getTime() > sleep_end ? "" : ", currently sleeping";
    browser.browserAction.setTitle({ tabId: tabId, title: "Unimpede " + domain + is_sleeping });
}

function updateIconUnimpeded(tabId, domain) {
    browser.runtime.getPlatformInfo(info => {
        if (info.os === browser.runtime.PlatformOs.ANDROID) return;
        var fastIcons;
        if (new Date().getTime() > sleep_end) {
            fastIcons = { 16: "icons/fast-16.png", 32: "icons/fast-32.png" };
        } else {
            fastIcons = { 16: "icons/fast-sleep-16.png", 32: "icons/fast-sleep-32.png" };
        }
        browser.browserAction.setIcon({ tabId: tabId, path: fastIcons });
    });
    var is_sleeping = new Date().getTime() > sleep_end ? "" : ", currently sleeping";
    browser.browserAction.setTitle({ tabId: tabId, title: "Impede " + domain + is_sleeping });
}
